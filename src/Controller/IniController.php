<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IniController extends AbstractController
{
    #[Route('/', name: 'blog_index')]
    public function index(): Response
    {

        // the template path is the relative file path from `templates/`
        return $this->render('ini/index.html.twig');
    }
}